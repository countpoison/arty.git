package arty

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/Unknwon/goconfig"
	"github.com/docker/docker/api/types"
	"github.com/docker/docker/api/types/container"
	"github.com/docker/docker/api/types/mount"
	"github.com/docker/docker/client"
	"github.com/docker/go-connections/nat"
)

//docker 环境初始化
func (p *GlobalConf) DockerInit() (*GlobalConf, error) {

	if p.Global.BaseConfigFileByte == nil {
		var err error
		p, err = p.CheckConfigFile()
		if err != nil {
			return nil, err
		}
	}
	os.Setenv("DOCKER_API_VERSION", p.DockerInitConfig.DOCKER_API_VERSION)
	p.DockerInitConfig.Ctx = context.Background()
	cli, err := client.NewClientWithOpts(client.FromEnv)
	if err != nil {
		ErrorOut(err)
		return nil, errors.New("connact Docker Failed")
	}
	defer cli.Close()
	p.DockerInitConfig.Cli = cli
	p.DockerInitConfig.Url, err = p.Global.BaseConfigFileByte.GetValue("global", "imageUrl")
	if err != nil {
		ErrorOut(err)
		return nil, err
	}
	p.DockerInitConfig.Tag, err = p.Global.BaseConfigFileByte.GetValue("global", "imageTag")
	if err != nil {
		ErrorOut(err)
		return nil, err
	}
	defer p.DockerInitConfig.Cli.Close()

	list := p.CheckImage(p.DockerInitConfig.Url + ":" + p.DockerInitConfig.Tag)
	if !list {
		list = p.PullImage(p.DockerInitConfig.Url + ":" + p.DockerInitConfig.Tag)
		if !list {
			err = errors.New("Pull image Failed")
			return nil, err
		}
	}
	p, err = p.ListContainer(true)
	if err != nil {
		return nil, err
	}
	return p, nil
}

//检查docker是否执行了初始化
func (p *GlobalConf) CheckDockerInit() (*GlobalConf, error) {
	if p.DockerInitConfig.Ctx == nil {
		p, err := p.DockerInit()
		if err != nil {
			ErrorOut(err)
			return nil, errors.New("Init Docker Failed")
		}
		return p, nil
	}
	return p, nil
}

func (p *GlobalConf) ArtyCreateDocker() (*GlobalConf, error) {
	var err error
	p, err = p.CreateBootNodeDocker()
	if err != nil {
		ErrorOut(err)
		return nil, err
	}
	for k, _ := range p.Nodes {
		p, err = p.CreateNodeDocker(k)
		if err != nil {
			ErrorOut(err)
			return nil, errors.New("func ArtyCreateDocker : Create Nodes Docker " + k + " Failed")
		}
	}
	return p, nil
}

//生成bootnode的docker容器配置
func (p *GlobalConf) CreateBootNodeDocker() (*GlobalConf, error) {
	p, err := p.CheckDockerInit()
	if p.BootNood.Id != "" {
		return p, nil
	}
	if err != nil {
		ErrorOut(err)
		return nil, errors.New("CreateBootNodeDocker : Docker Init Failed")
	}

	bootNodePortlist := []string{p.BootNood.Port}
	mountlist := make(map[string]string)
	mountlist[p.Global.BootNodeDir] = "/opt/node"

	config, HostConfig := p.ContainerConfig(bootNodePortlist, mountlist)
	body, err := p.DockerInitConfig.Cli.ContainerCreate(p.DockerInitConfig.Ctx, config, HostConfig, nil, "bootnode")
	if err != nil {
		ErrorOut(err)
		return nil, errors.New("CreateBootNodeDocker : Configure BootNode Docker Failed")
	}
	p.BootNood.Id = body.ID
	return p, nil
}

//生成node的docker容器配置
func (p *GlobalConf) CreateNodeDocker(node string) (*GlobalConf, error) {
	p, err := p.CheckDockerInit()
	if err != nil {
		ErrorOut(err)
		return nil, errors.New("CreateBootNodeDocker : Docker Init Failed")
	}

	if p.Nodes[node].Id != "" {
		return p, nil
	}

	portlist := []string{}
	port, err := p.Global.BaseConfigFileByte.GetValue(node, "port")
	if err != nil {
		ErrorOut(err)
		return nil, errors.New("CreateNodeDocker : read config [" + node + "] 'prot' Failed")
	}
	rpcport, err := p.Global.BaseConfigFileByte.GetValue(node, "rpcport")
	if err == nil {
		portlist = append(portlist, rpcport)
	}

	portlist = append(portlist, port)

	mountlist := make(map[string]string)
	mountlist[p.Global.NodesDir+"/"+node] = "/opt/node"
	mountlist[p.Global.BootNodeDir+"/boot.json"] = "/opt/node/genesis.json"

	config, HostConfig := p.ContainerConfig(portlist, mountlist)
	body, err := p.DockerInitConfig.Cli.ContainerCreate(p.DockerInitConfig.Ctx, config, HostConfig, nil, node)
	if err != nil {
		ErrorOut(err)
		return nil, errors.New("CreateNodeDocker : Create node Container " + node + " Failed")
	}
	p.Nodes[node].Id = body.ID
	return p, nil

}

//生成Container的配置文件,config和hostconfig
func (p *GlobalConf) ContainerConfig(postlist []string, mountlist map[string]string) (*container.Config, *container.HostConfig) {
	HostConfig := &container.HostConfig{}
	a, b := Addport(postlist)
	HostConfig.PortBindings = a

	addmount := Addmount(mountlist)
	HostConfig.Mounts = addmount

	config := &container.Config{}
	config.Image = p.DockerInitConfig.Url + ":" + p.DockerInitConfig.Tag
	config.Tty = true
	config.OpenStdin = true
	config.Cmd = []string{"/opt/bin/auto.sh"}
	config.ExposedPorts = b

	return config, HostConfig
}

//添加portlist
func Addport(portlist []string) (nat.PortMap, nat.PortSet) {
	portMap := nat.PortMap{}
	exposedPorts := nat.PortSet{}
	for _, v := range portlist {
		udpport, _ := nat.NewPort("udp", v)
		tcpport, _ := nat.NewPort("tcp", v)
		tmp := make([]nat.PortBinding, 0, 1)
		portBind := nat.PortBinding{HostIP: "0.0.0.0", HostPort: v}
		tmp = append(tmp, portBind)

		portMap[udpport] = tmp
		portMap[tcpport] = tmp
		exposedPorts[udpport] = struct{}{}
		exposedPorts[tcpport] = struct{}{}
	}
	return portMap, exposedPorts
}

//添加mountlist,返回符合ContainerConfig使用的结构
func Addmount(mountlist map[string]string) []mount.Mount {
	mounts := make([]mount.Mount, 0, 1)
	for k, v := range mountlist {
		mount := mount.Mount{
			Type:   mount.TypeBind,
			Source: k,
			Target: v,
		}
		mounts = append(mounts, mount)
	}
	return mounts
}

//启动全部docker,根据全局配置文件
func (p *GlobalConf) DockerStart() (*GlobalConf, error) {
	p, err := p.CheckDockerInit()
	if err != nil {
		ErrorOut(err)
		return nil, errors.New("DockerStart : Docker Init Failed")
	}

	if p.BootNood.State != "running" && p.BootNood.Start {

		err = p.DockerInitConfig.Cli.ContainerStart(p.DockerInitConfig.Ctx, p.BootNood.Id, types.ContainerStartOptions{})
		if err != nil {
			ErrorOut(err)
			return nil, err
		}
	}

	for k, v := range p.Nodes {
		if p.Nodes[k].Id != "running" && p.Nodes[k].Start {
			p, err = p.EditBootStrapNodes(k)
			if err != nil {
				ErrorOut(err)
				return nil, err
			}
			p.DockerInitConfig.Cli.ContainerStart(p.DockerInitConfig.Ctx, v.Id, types.ContainerStartOptions{})
		}
	}
	p, err = p.ListContainer(true)
	if err != nil {
		return nil, err
	}
	return p, nil
}

//启动单个docker 
func (p *GlobalConf) StartContainerSingle(ID string) (*GlobalConf, error) {
	p, err := p.CheckDockerInit()
	if err != nil {
		ErrorOut(err)
		return nil, errors.New("DockerStart : Docker Init Failed")
	}

	err = p.DockerInitConfig.Cli.ContainerStart(p.DockerInitConfig.Ctx, ID, types.ContainerStartOptions{})
	if err != nil {
		return nil, err
	}
	return p, nil
	
}



//修改bootstrapNodes信息 广播地址
func (p *GlobalConf) EditBootStrapNodes(node string) (*GlobalConf, error) {
	BootstrapNodes, err := p.Global.BaseConfigFileByte.GetValue("global", "BootstrapNodes")
	if err != nil {
		for true {
			Exist := p.PathExistsFile(p.Global.BootNodeDir + "/enode.txt")
			if Exist {
				time.Sleep(1 * time.Second)
				break
			}
			time.Sleep(1 * time.Second)
		}
		f, err := ioutil.ReadFile(p.Global.BootNodeDir + "/enode.txt")
		if err != nil {
			ErrorOut(err)
			return nil, errors.New("EditBootStrapNodes : read enode.txt failed")
		}
		coinBase, err := p.Global.BaseConfigFileByte.GetValue("global", "coinBase")
		if err != nil {
			ErrorOut(err)
			return nil, errors.New("EditBootStrapNodes : read get coinbase failed")
		} else {
			if coinBase == "0x0000000000000000000000000000000000000000" {
				var genesis Genesis
				for true {
					genesisstr, _ := ioutil.ReadFile(p.Global.BootNodeDir + "/boot.json")
					err = json.Unmarshal(genesisstr, &genesis)
					if err != nil {
						return nil, err
					}
					if genesis.Coinbase != "0x0000000000000000000000000000000000000000" {
						break
					}
				}
				p.Global.CoinBase = genesis.Coinbase
				p.Global.BaseConfigFileByte.SetValue("global", "coinBase", genesis.Coinbase)
				goconfig.SaveConfigFile(p.Global.BaseConfigFileByte, p.Global.BaseConfigFile)
			}
		}
		enode := "enode://" + strings.Replace(string(f), "\n", "", -1) + "@" + p.BootNood.IP + ":" + p.BootNood.Port
		var enodes []string
		enodes = append(enodes, enode)
		enodes_str, _ := json.Marshal(enodes)
		BootstrapNodes = string(enodes_str)
		p.Global.BaseConfigFileByte.SetValue("global", "BootstrapNodes", string(enodes_str))
		goconfig.SaveConfigFile(p.Global.BaseConfigFileByte, p.Global.BaseConfigFile)
	}
	_, err = p.Nodes[node].NodeIniConfig.GetValue("Eth", "Etherbase")
	if err != nil {
		p.Nodes[node].NodeIniConfig.SetValue("Eth", "Etherbase", "\""+p.Global.CoinBase+"\"")
	}
	p.Nodes[node].NodeIniConfig.SetValue("Node.P2P", "BootstrapNodes", BootstrapNodes)
	p.Nodes[node].NodeIniConfig.SetValue("Node.P2P", "BootstrapNodesV5", BootstrapNodes)
	ioutil.WriteFile(p.Global.NodesDir+"/"+node+"/cmdstr.txt", []byte(p.Nodes[node].Cmdstr), 0644)
	goconfig.SaveConfigFile(p.Nodes[node].NodeIniConfig, p.Global.NodesDir+"/"+node+"/conf.ini")
	return p, nil
}

//列出配置文件中所有docker容器
func (p *GlobalConf) ListContainer(all bool) (*GlobalConf, error) {
	p, err := p.CheckDockerInit()
	if err != nil {
		ErrorOut(err)
		return nil, errors.New("ListContainer : Docker Init Failed")
	}
	containers, err := p.DockerInitConfig.Cli.ContainerList(p.DockerInitConfig.Ctx, types.ContainerListOptions{All: all})
	if err != nil {
		ErrorOut(err)
		return nil, errors.New("List containers Errors")
	}

	for _, container := range containers {
		str := strings.Replace(container.Names[0], "/", "", -1)
		for k, _ := range p.Nodes {
			if str == k {
				p.Nodes[k].Created = container.Created
				p.Nodes[k].Id = container.ID
				p.Nodes[k].State = container.State
				p.Nodes[k].Status = container.Status
			}
			if str == "bootnode" {
				p.BootNood.Created = container.Created
				p.BootNood.Id = container.ID
				p.BootNood.State = container.State
				p.BootNood.Status = container.Status
			}
		}
	}
	return p, nil
}

//列出所有镜像
func (p *GlobalConf) ListImages() ([]map[string]string, error) {
	p, err := p.CheckDockerInit()
	if err != nil {
		ErrorOut(err)
		return nil, errors.New("CreateBootNodeDocker : Docker Init Failed")
	}
	list := []map[string]string{}
	images, err := p.DockerInitConfig.Cli.ImageList(p.DockerInitConfig.Ctx, types.ImageListOptions{})
	if err != nil {
		ErrorOut(err)
		return nil, errors.New("List ImageList Errors")
	}
	for _, image := range images {
		imageinfo := map[string]string{}
		imageinfo["ID"] = image.ID
		imageinfo["Created"] = strconv.FormatInt(image.Created, 10)
		imageinfo["Size"] = strconv.FormatInt(image.Size, 10)
		imageinfo["Containers"] = strconv.FormatInt(image.Containers, 10)
		imageinfo["ParentID"] = image.ParentID
		imageinfo["RepoTags"] = image.RepoTags[0]
		imageinfo["SharedSize"] = strconv.FormatInt(image.SharedSize, 10)
		imageinfo["VirtualSize"] = strconv.FormatInt(image.VirtualSize, 10)
		list = append(list, imageinfo)
	}
	return list, nil

}

//检查配置文件中的镜像是否存在
func (p *GlobalConf) CheckImage(imagename string) bool {
	p, err := p.CheckDockerInit()
	if err != nil {
		ErrorOut(err)
		return false
	}
	images, err := p.DockerInitConfig.Cli.ImageList(p.DockerInitConfig.Ctx, types.ImageListOptions{All: true})
	if err != nil {
		ErrorOut(err)
		return false
	}
	for _, image := range images {
		if image.RepoTags[0] == imagename {
			return true
		}
	}
	return false
}

//下载配置文件中的镜像
func (p *GlobalConf) PullImage(imagename string) bool {
	p, err := p.CheckDockerInit()
	if err != nil {
		ErrorOut(err)
		return false
	}
	out, err := p.DockerInitConfig.Cli.ImagePull(p.DockerInitConfig.Ctx, imagename, types.ImagePullOptions{})
	if err != nil {
		ErrorOut(err)
		return false
	}
	defer out.Close()
	io.Copy(os.Stdout, out)
	return true
}

//停止一个容器
func (p *GlobalConf) StopDocker(containername string) bool {
	var err error
	if p.DockerInitConfig.Cli == nil {
		p, err = p.DockerInit()
		if err != nil {
			ErrorOut(err)
			return false
		}
	}
	containerid, state := p.NameToID(containername)
	if containerid == "" {
		return false
	}
	if state != "running" {
		return true
	}
	err = p.DockerInitConfig.Cli.ContainerStop(p.DockerInitConfig.Ctx, containerid, nil)
	if err != nil {
		ErrorOut(err)
		return false
	}
	return true
}

//删除一个容器,强制关闭(请确认是否删除数据目录)
func (p *GlobalConf) RemoveDocker(containername string, all bool) bool {
	var err error
	if p.DockerInitConfig.Cli == nil {
		p, err = p.DockerInit()
		if err != nil {
			ErrorOut(err)
			return false
		}
	}

	containerid, state := p.NameToID(containername)
	if containerid == "" {
		return false
	}
	if state == "running" {
		stopok := p.StopDocker(containername)
		if !stopok {
			return false
		}
	}

	err = p.DockerInitConfig.Cli.ContainerRemove(p.DockerInitConfig.Ctx, containerid, types.ContainerRemoveOptions{})
	if err != nil {
		ErrorOut(err)
		return false
	}
	if all {
		err = os.RemoveAll(p.Global.NodesDir + "/" + containername)
		if err != nil {
			fmt.Println(p.Global.NodesDir + "/" + containername + ",deleted error")
			return false
		}
	}
	return true
}

//容器名称转id
func (p *GlobalConf) NameToID(containername string) (string, string) {
	var err error
	if p.DockerInitConfig.Cli == nil {
		p, err = p.DockerInit()
		if err != nil {
			ErrorOut(err)
			return "", ""
		}
	}
	containers, err := p.DockerInitConfig.Cli.ContainerList(p.DockerInitConfig.Ctx, types.ContainerListOptions{All: true})
	if err != nil {
		ErrorOut(err)
		return "", ""
	}
	for _, v := range containers {
		if strings.Replace(v.Names[0], "/", "", -1) == containername {
			return v.ID, v.State
		}
	}
	return "", ""
}

//重建容器,强制关闭和删除,保留数据(确认是否保留数据)
func (p *GlobalConf) RebuildDocker(containername string, all bool) (*GlobalConf, error) {
	var err error
	p, err = p.CheckConfigFile()
	delok := p.RemoveDocker(containername, all)
	if !delok {
		return nil, errors.New("delete " + containername + " error")
	}
	p.Nodes[containername].Id = ""
	p, err = p.CreateNodeDocker(containername)
	if err != nil {
		return nil, err
	}
	p, err = p.EditBootStrapNodes(containername)
	if err != nil {
		return nil, err
	}
	id, _ := p.NameToID(containername)
	p.DockerInitConfig.Cli.ContainerStart(p.DockerInitConfig.Ctx, id, types.ContainerStartOptions{})
	return p, nil

}

//删除容器全部相关信息
func (p *GlobalConf) DeletedockerAll(containername string) bool {
	var err error
	p, err = p.CheckConfigFile()
	if err != nil {
		return false
	}
	delok := p.RemoveDocker(containername, true)
	if !delok {
		return false
	}
	delok = p.DeleteNodeConfig(containername)
	if delok {
		return true
	}
	return false
}
