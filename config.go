package arty

import (
	"errors"
	"strconv"

	"github.com/Unknwon/goconfig"
)

//global conf.ini文件是否存在，不存在则创建默认配置文件
func (p *GlobalConf) LoadConfigFile() (*GlobalConf, error) {
	var err error

	if p.Global.BaseConfigFile == "" {
		p, err = Init()
		if err != nil {
			return nil, err
		}
	}
	//从文件读取global conf.ini文件
	p.Global.BaseConfigFileByte, err = goconfig.LoadConfigFile(p.Global.BaseConfigFile)
	if err != nil {
		ErrorOut(err)
		return nil, errors.New("LoadConfigFile Failed ,LoadFile " + p.Global.BaseConfigFile + " Failed")
	}

	//读取wordir目录
	p.Global.WorkDir, err = p.Global.BaseConfigFileByte.GetValue("global", "workDir")
	if err != nil {
		ErrorOut(err)
		return nil, errors.New("LoadConfigFile Failed ,Read [global] 'workDir'  Failed")
	}

	//读取bootnode的ip
	p.BootNood.IP, err = p.Global.BaseConfigFileByte.GetValue("global", "bootNodeIP")
	if err != nil {
		ErrorOut(err)
		return nil, errors.New("LoadConfigFile Failed ,Read [global] 'bootNodeIP'  Failed")
	}

	//读取networkid
	p.Global.NetworkId, err = p.Global.BaseConfigFileByte.GetValue("global", "NetworkId")
	if err != nil {
		ErrorOut(err)
		return nil, errors.New("LoadConfigFile Failed ,Read [global] 'NetworkId'  Failed")
	}
	//去读coinbase地址
	p.Global.CoinBase, err = p.Global.BaseConfigFileByte.GetValue("global", "coinBase")
	if err != nil {
		ErrorOut(err)
		return nil, errors.New("LoadConfigFile Failed ,Read [global] 'coinBase'  Failed")
	}
	//读取bootnode的端口
	p.BootNood.Port, err = p.Global.BaseConfigFileByte.GetValue("global", "bootNodePort")
	if err != nil {
		ErrorOut(err)
		return nil, errors.New("LoadConfigFile Failed ,Read [global] 'bootNodePort'  Failed")
	}
	//读取bootnode是否启动
	bootNodeStart, err := p.Global.BaseConfigFileByte.GetValue("global", "bootNodeStart")
	if err != nil {
		ErrorOut(err)
		return nil, errors.New("LoadConfigFile Failed ,Read [global] 'Start'  Failed")
	}
	p.BootNood.Start, err = strconv.ParseBool(bootNodeStart)
	if err != nil {
		ErrorOut(err)
		return nil, errors.New("strconv Failed ,Read [global] 'Start'  Failed")
	}

	p.DockerInitConfig.DOCKER_API_VERSION, err = p.Global.BaseConfigFileByte.GetValue("global", "DOCKER_API_VERSION")
	if err != nil {
		ErrorOut(err)
		return nil, errors.New("LoadConfigFile Failed ,Read [global] 'DOCKER_API_VERSION'  Failed")
	}

	//创建关键目录
	p.Global.NodesDir = p.Global.WorkDir + "/nodes"
	p.Global.BootNodeDir = p.Global.WorkDir + "/bootnode"
	p.PathExistsDir(p.Global.WorkDir)
	p.PathExistsDir(p.Global.NodesDir)
	p.PathExistsDir(p.Global.BootNodeDir)

	//修改genesis.json文件的关键信息
	NetworkIdInt, err := strconv.Atoi(p.Global.NetworkId)
	if err != nil {
		ErrorOut(err)
		return nil, errors.New("NetworkId strconv Failed")
	}
	p.BootNood.Genesis.Config.ChainId = NetworkIdInt
	p.BootNood.Genesis.Coinbase = p.Global.CoinBase

	//获取nodes节点名称，并初始化
	allSections := p.Global.BaseConfigFileByte.GetSectionList()

	p, err = p.NodeRegrep(allSections)
	if err != nil {
		ErrorOut(err)
		return nil, err
	}

	//初始化genesis.json文件
	err = p.CreateGenesisJson()
	if err != nil {
		ErrorOut(err)
		return nil, err
	}

	return p, nil
}

//检查配置文件是否符合标准,并且检查创建关键目录
func (p *GlobalConf) CheckConfigFile() (*GlobalConf, error) {
	var err error

	p, err = p.LoadConfigFile()
	if err != nil {
		return nil, err
	}

	//检查bootnode是否符合启动规则，BootstrapNodes和bootNodeStart必须有一项是启动的
	_, err = p.Global.BaseConfigFileByte.GetValue("global", "BootstrapNodes")
	if err != nil && !p.BootNood.Start {
		err = errors.New("[global] 'BootstrapNodes' or 'bootNodeStart' must be have 1")
		ErrorOut(err)
		return nil, err
	}

	//检查端口是否有重复
	portlist := map[string]int{}
	portlist[p.BootNood.Port] = 1
	for _, v := range p.Nodes {
		portlist[v.Port] = portlist[v.Port] + 1
		if portlist[v.Port] > 1 {
			err = errors.New("Port duplication")
			ErrorOut(err)
			return nil, err
		}
	}

	return p, nil
}

//获取新增的node名称
func (p *GlobalConf) GetAddNodeNum() string {
	var err error
	if p.Global.BaseConfigFileByte == nil {
		p, err = p.LoadConfigFile()
		if err != nil {
			return ""
		}
	}
	allSections := p.Global.BaseConfigFileByte.GetSectionList()
	i := 0
	var nodename string
	nodes := map[string]int{}
	for i < len(allSections) {
		str := "node." + strconv.Itoa(i)
		nodes[str] = nodes[str] + 1
		i = i + 1
	}
	for _, v := range allSections {
		if v != "global" {
			nodes[v] = nodes[v] + 1
		}
	}
	for k, s := range nodes {
		if s == 1 {
			nodename = k
			break
		}

	}
	return nodename
}

//新增node节点，写入conf.ini
func (p *GlobalConf) AddNodeConfig(conf map[string]string) (*GlobalConf, error) {
	node := p.GetAddNodeNum()
	if node == "" {
		return nil, errors.New("Node Get New Failed")
	}
	for k, v := range conf {
		boo := p.Global.BaseConfigFileByte.SetValue(node, k, v)
		if !boo {
			return nil, errors.New("jbooooooooooo")
		}
	}
	var err error

	goconfig.SaveConfigFile(p.Global.BaseConfigFileByte, p.Global.BaseConfigFile)

	p, err = p.CheckConfigFile()
	if err != nil {
		ErrorOut(err)
		return nil, err
	}
	return p, nil

}

//删除node节点，写入conf.ini
func (p *GlobalConf) DeleteNodeConfig(nodename string) bool {
	var err error
	if p.Global.BaseConfigFileByte == nil {
		p, err = p.LoadConfigFile()
		if err != nil {
			return false
		}
	}
	p.Global.BaseConfigFileByte.DeleteSection(nodename)
	goconfig.SaveConfigFile(p.Global.BaseConfigFileByte, p.Global.BaseConfigFile)
	return true

}

//修改全局配置
//只允许启动前修改，可支持的修改参数为：bootNodeStart，bootNodePort，bootNodeIP，coinBase，NetworkId，imageUrl,imageTag,DOCKER_API_VERSION,BootstrapNodes
func (p *GlobalConf) SetGlobalconfig(edit map[string]string) (*GlobalConf, error) {
	for k, v := range edit {
		p.Global.BaseConfigFileByte.SetValue("global", k, v)
	}
	goconfig.SaveConfigFile(p.Global.BaseConfigFileByte, p.Global.BaseConfigFile)
	return p, nil
}
