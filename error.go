package arty

import (
	"log"
	"os"
)

//错误检查 直接退出
func ErrorOut(err error) {
	var logger *log.Logger
	file, errs := os.OpenFile("test.log", os.O_APPEND|os.O_CREATE, 666)
	if errs != nil {
		log.Fatalln("fail to create test.log file!")
	}
	defer file.Close()
	logger = log.New(file, "", log.LstdFlags|log.Lshortfile)
	logger.SetFlags(log.LstdFlags)
	logger.Println(err)
	log.Println(err)

}
