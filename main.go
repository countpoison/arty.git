package arty

func (p *GlobalConf) Exec() (*GlobalConf, error) {
	//创建docker容器
	var err error
	p, err = p.ArtyCreateDocker()
	if err != nil {
		return nil, err
	}
	p, err = p.DockerStart()
	if err != nil {
		return nil, err
	}
	return p, nil
}

func (p *GlobalConf) AutoExec() (*GlobalConf, error) {

	var err error

	//检查配置文件，自动加载conf.ini配置文件
	p, err = p.CheckConfigFile()
	if err != nil {
		return nil, err
	}
	//创建docker容器
	p, err = p.DockerInit()
	if err != nil {
		return nil, err
	}
	p, err = p.ArtyCreateDocker()
	if err != nil {
		return nil, err
	}
	p, err = p.DockerStart()
	if err != nil {
		return nil, err
	}
	return p, nil
}
