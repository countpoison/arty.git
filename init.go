package arty

import (
	"errors"
	"runtime"

	"github.com/Unknwon/goconfig"
)

func Init() (*GlobalConf, error) {

	//定义全局结构，这一步很重要
	globalconf := new(GlobalConf)
	globalconf.Nodes = make(map[string]*NodesConfig)

	ostype := runtime.GOOS
	if ostype == "windows" {
		globalconf.Global.BaseDir = "./conf"
		globalconf.Global.WorkDir = "./workdir"
	} else if ostype == "linux" {
		globalconf.Global.BaseDir = "/etc/arty"
		globalconf.Global.WorkDir = "/opt/arty"
	} else {
		err := errors.New("Unrecognized System")
		ErrorOut(err)
		return nil, err
	}

	globalconf.Global.BaseConfigFile = globalconf.Global.BaseDir + "/arty.ini"
	globalconf.PathExistsDir(globalconf.Global.BaseDir)
	globalconf.Global.LocalIP = globalconf.GetLocalIp()
	globalconf = globalconf.DFileContentsVal()

	filestatus := globalconf.PathExistsFile(globalconf.Global.BaseConfigFile)
	if !filestatus {
		cfg, _ := goconfig.LoadFromData(globalconf.Global.DCFileContents)
		err := goconfig.SaveConfigFile(cfg, globalconf.Global.BaseConfigFile)
		if err != nil {
			ErrorOut(err)
			return nil, errors.New("Init Failed ,SaveFile " + globalconf.Global.BaseConfigFile + " Failed")
		}
	}
	return globalconf, nil
}
