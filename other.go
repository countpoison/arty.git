package arty

import (
	"encoding/json"
	"errors"
	"io/ioutil"
	"net"
	"os"
	"regexp"
	"strconv"

	"github.com/Unknwon/goconfig"
)

//获取本机IP
func (p *GlobalConf) GetLocalIp() string {
	addrs, err := net.InterfaceAddrs()
	if err != nil {
		return "127.0.0.1"
	}
	for _, address := range addrs {
		if ipnet, ok := address.(*net.IPNet); ok && !ipnet.IP.IsLoopback() {
			if ipnet.IP.To4() != nil {
				return ipnet.IP.String()
			}
		}
	}
	return "127.0.0.1"
}

//启动则加载默认配置文件
func (p *GlobalConf) DFileContentsVal() *GlobalConf {
	p.Global.DCFileContents = []byte(
		`[global]
workDir = ` + p.Global.WorkDir + `
bootNodeStart = true
bootNodePort = 30301
bootNodeIP = ` + p.Global.LocalIP + `
coinBase = 0x0000000000000000000000000000000000000000
bootNodeCheckStart = false
NetworkId = 110
imageUrl = hub.c.163.com/lurktion/arty
imageTag = wandering
DOCKER_API_VERSION = 1.39

[node.0]
port = 30303
mine = true
Start = true
HTTPModules = ["net","web3", "eth", "shh","admin"]
minerthreads = 2
`)

	p.Global.NCFileContents = []byte(
		`[Eth] 
NetworkId = 1 
SyncMode = "fast" 
NoPruning = false 
LightPeers = 100 
DatabaseCache = 512 
TrieCleanCache = 256 
TrieDirtyCache = 256 
TrieTimeout = 3600000000000 
MinerGasFloor = 8000000 
MinerGasCeil = 8000000 
MinerGasPrice = 1000000000 
MinerRecommit = 3000000000 
MinerNoverify = false 
EnablePreimageRecording = false 
EWASMInterpreter = "" 
EVMInterpreter = "" 
[Eth.Ethash] 
CacheDir = "ethash" 
CachesInMem = 2 
CachesOnDisk = 3 
DatasetDir = "/root/.ethash" 
DatasetsInMem = 1 
DatasetsOnDisk = 2 
PowMode = 0 
[Eth.TxPool] 
Locals = [] 
NoLocals = false 
Journal = "transactions.rlp" 
Rejournal = 3600000000000 
PriceLimit = 1 
PriceBump = 10 
AccountSlots = 16 
GlobalSlots = 4096 
AccountQueue = 64 
GlobalQueue = 1024 
Lifetime = 10800000000000 
[Eth.GPO] 
Blocks = 20 
Percentile = 60 
[Shh] 
MaxMessageSize = 1048576 
MinimumAcceptedPOW = 2e-01 
RestrictConnectionBetweenLightClients = true 
[Node] 
DataDir = "/opt/node" 
IPCPath = "geth.ipc" 
HTTPPort = 8545 
HTTPVirtualHosts = ["localhost"] 
HTTPModules = [] 
WSPort = 8546 
WSModules = [] 
[Node.P2P] 
MaxPeers = 25 
NoDiscovery = false 
BootstrapNodes = []  
BootstrapNodesV5 = [] 
StaticNodes = [] 
TrustedNodes = [] 
ListenAddr = ":30303" 
EnableMsgEvents = false 
[Node.HTTPTimeouts] 
ReadTimeout = 30000000000 
WriteTimeout = 30000000000 
IdleTimeout = 120000000000 
[Dashboard] 
Host = "localhost" 
Port = 8080 
Refresh = 5000000000`)

	p.BootNood.Genesis.Config.HomesteadBlock = 1
	p.BootNood.Genesis.Config.Eip150Block = 2
	p.BootNood.Genesis.Config.Eip150Hash = "0x0000000000000000000000000000000000000000000000000000000000000000"
	p.BootNood.Genesis.Config.Eip155Block = 3
	p.BootNood.Genesis.Config.Eip158Block = 3
	p.BootNood.Genesis.Config.ByzantiumBlock = 4
	p.BootNood.Genesis.Config.ByzantiumBlock = 5
	p.BootNood.Genesis.Nonce = "0x0"
	p.BootNood.Genesis.Timestamp = "0x5c3ef58f"
	p.BootNood.Genesis.ExtraData = "0x0000000000000000000000000000000000000000000000000000000000000000"
	p.BootNood.Genesis.GasLimit = "0x47b760"
	p.BootNood.Genesis.Difficulty = "0x80000"
	p.BootNood.Genesis.MixHash = "0x0000000000000000000000000000000000000000000000000000000000000000"
	p.BootNood.Genesis.Number = "0x0"
	p.BootNood.Genesis.GasLimit = "0x0"
	p.BootNood.Genesis.ParentHash = "0x0000000000000000000000000000000000000000000000000000000000000000"
	p.BootNood.Genesis.Alloc = map[string]string{}

	return p
}

//检查目录是否存在，不存在则创建
func (p *GlobalConf) PathExistsDir(path string) error {
	_, err := os.Stat(path)
	if os.IsNotExist(err) {
		errs := os.Mkdir(path, os.ModePerm)
		if errs != nil {
			return errs
		}
		return nil
	} else {
		return nil
	}
	return nil
}

//检查文件是否存在
func (p *GlobalConf) PathExistsFile(path string) bool {
	_, err := os.Stat(path)
	if err == nil {
		return true
	}
	if os.IsNotExist(err) {
		return false
	}
	return false
}

//生成genesis.json文件
func (p *GlobalConf) CreateGenesisJson() error {
	genesis_json_file, err := json.Marshal(p.BootNood.Genesis)
	if err != nil {
		ErrorOut(err)
		return errors.New("genesis_json Failed")
	}
	ioutil.WriteFile(p.Global.BootNodeDir+"/boot.json", genesis_json_file, 0644)
	return nil
}

//过滤conf.ini 里面所有Section,匹配符合规则的node标签写法
func (p *GlobalConf) NodeRegrep(nodedirs []string) (*GlobalConf, error) {
	for _, v := range nodedirs {
		match, err := regexp.MatchString("^node\\.\\d+$", v)
		if err != nil {
			ErrorOut(err)
			return nil, errors.New("Regrep Node " + v + "Dir Name Failed")
		}
		if match {
			//创建nodes.N自己的目录
			p.PathExistsDir(p.Global.NodesDir + "/" + v)
			nodeconf, err := p.Node_conf(v)
			if err != nil {
				ErrorOut(err)
				return nil, err
			}
			p.Nodes[v] = nodeconf

		} else {
			if v != "global" {
				err = errors.New("Contains nonstandard labels")
				ErrorOut(err)
				return nil, err
			}
		}
	}
	return p, nil
}

//配置node自己的conf.ini文件的方法
func (p *GlobalConf) Node_conf(node string) (*NodesConfig, error) {
	list := NodesConfig{}

	node_toml, _ := goconfig.LoadFromData(p.Global.NCFileContents)
	networkid, err := p.Global.BaseConfigFileByte.GetValue("global", "NetworkId")
	if err != nil {
		node_toml.SetValue("Eth", "NetworkId", p.Global.NetworkId)
	} else {
		node_toml.SetValue("Eth", "NetworkId", networkid)
	}

	HTTPModules, err := p.Global.BaseConfigFileByte.GetValue(node, "HTTPModules")
	if err == nil {
		node_toml.SetValue("Node", "HTTPModules", HTTPModules)
	} else {
		node_toml.SetValue("Node", "HTTPModules", "[\"net\", \"web3\", \"eth\", \"shh\"]")
	}
	rpcport, err := p.Global.BaseConfigFileByte.GetValue(node, "rpcport")
	if err == nil {
		node_toml.SetValue("Node", "HTTPPort", rpcport)
		node_toml.SetValue("Node", "HTTPHost", "\"0.0.0.0\"")
	}

	list.Port, err = p.Global.BaseConfigFileByte.GetValue(node, "port")
	if err != nil {
		ErrorOut(err)
		return nil, errors.New("Get Node " + node + " Port Failed")
	}

	//奖励地址
	Etherbase, err := p.Global.BaseConfigFileByte.GetValue(node, "Etherbase")
	if err == nil {
		node_toml.SetValue("Eth", "Etherbase", "\""+Etherbase+"\"")
	} else {
		if p.Global.CoinBase != "0x0000000000000000000000000000000000000000" {
			node_toml.SetValue("Eth", "Etherbase", "\""+p.Global.CoinBase+"\"")
		} else {
			node_toml.DeleteKey("Eth", "Etherbase")
		}
	}

	nodeStart, err := p.Global.BaseConfigFileByte.GetValue(node, "Start")
	if err != nil {
		ErrorOut(err)
		return nil, errors.New("Get Node " + node + " Start Failed")
	}
	list.Start, err = strconv.ParseBool(nodeStart)
	if err != nil {
		ErrorOut(err)
		return nil, errors.New("strconv Failed ,Read [" + node + "] 'Start'  Failed")
	}

	node_toml.SetValue("Node.P2P", "ListenAddr", "\":"+list.Port+"\"")
	list.NodeIniConfig = node_toml
	list.Cmdstr = node_cmdstr(p.Global.BaseConfigFileByte, node)
	return &list, nil
}

//启动脚本
func node_cmdstr(p *goconfig.ConfigFile, node string) string {
	cmdstr := ""
	mine, err := p.Bool(node, "mine")
	if err == nil {
		if mine {
			minerthreads, err := p.GetValue(node, "minerthreads")
			if err == nil {
				cmdstr = cmdstr + " --mine --minerthreads " + minerthreads
			} else {
				cmdstr = cmdstr + " --mine 1"
			}
		}
	}

	bootNodeIP, err := p.GetValue("global", "bootNodeIP")
	cmdstr = cmdstr + " --nat extip:" + bootNodeIP
	return cmdstr
}
