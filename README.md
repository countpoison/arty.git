# arty

#### 介绍

arty 是采用go语言解析配置文件的形式 自动生成geth的配置文件，并创建相应目录
通过docker go语言的sdk 管理docker的创建，运行管理等操作

#### 下载说明

go get gitee.com/countpoison/arty.git

#### 使用说明

```
package main

import (
	"gitee.com/countpoison/arty.git"
)

func main() {
	//完全自动执行，遇到错误会退出
	p, err := arty.Init()
	if err != nil {
		fmt.Println(err)
	}
	_, err = p.AutoExec()
	if err != nil {
		fmt.Println(err)
	}

}
```

#### 配置文件字段含义说明
见wiki


#### 镜像仓库地址
镜像名称
hub.c.163.com/lurktion/arty
版本号
wandering