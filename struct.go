package arty

import (
	"context"

	"github.com/Unknwon/goconfig"
	"github.com/docker/docker/client"
)

//genesis.json的结构体
type Genesis struct {
	Config     Config            `json:"config,omitempty"`
	Nonce      string            `json:"nonce,omitempty"`
	Timestamp  string            `json:"timestamp,omitempty"`
	ExtraData  string            `json:"extraData,omitempty"`
	GasLimit   string            `json:"gasLimit,omitempty"`
	Difficulty string            `json:"difficulty,omitempty"`
	MixHash    string            `json:"mixHash,omitempty"`
	Coinbase   string            `json:"coinbase,omitempty"`
	Alloc      map[string]string `json:"alloc"`
	Number     string            `json:"number,omitempty"`
	GasUsed    string            `json:"gasUsed,omitempty"`
	ParentHash string            `json:"parentHash,omitempty"`
}

type Config struct {
	ChainId             int    `json:"chainId,omitempty"`
	HomesteadBlock      int    `json:"homesteadBlock,omitempty"`
	Eip150Block         int    `json:"eip150Block,omitempty"`
	Eip150Hash          string `json:"eip150Hash,omitempty"`
	Eip155Block         int    `json:"eip155Block,omitempty"`
	Eip158Block         int    `json:"eip158Block,omitempty"`
	ByzantiumBlock      int    `json:"byzantiumBlock,omitempty"`
	ConstantinopleBlock int    `json:"constantinopleBlock,omitempty"`
	Ethash              Ethash `json:"ethash,omitempty"`
}

type Ethash struct {
	Ethash string `json:"ethash,omitempty"`
}

//整体数据的结构体
type GlobalConf struct {
	Global           GlobalSet
	BootNood         BootNoodConfig
	Nodes            map[string]*NodesConfig
	DockerInitConfig DockerInitConfig
}

//全局配置结构
type GlobalSet struct {
	WorkDir            string
	BaseDir            string
	BootNodeDir        string
	NodesDir           string
	BaseConfigFile     string
	BaseConfigFileByte *goconfig.ConfigFile
	LocalIP            string
	NetworkId          string
	CoinBase           string
	DCFileContents     []byte
	NCFileContents     []byte
}

//bootnode 配置结构
type BootNoodConfig struct {
	Id      string
	Start   bool
	Status  string
	Port    string
	State   string
	IP      string
	Created int64
	Genesis Genesis
}

//node节点配置结构
type NodesConfig struct {
	Id            string
	Start         bool
	State         string
	Created       int64
	Status        string
	Cmdstr        string
	Port          string
	Etherbase     string
	NodeIniConfig *goconfig.ConfigFile
}

//docker需要的数据初始化
type DockerInitConfig struct {
	Ctx                context.Context
	Cli                *client.Client
	Url                string
	Tag                string
	DOCKER_API_VERSION string
}
